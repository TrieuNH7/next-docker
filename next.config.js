module.exports = {
  experimental: {
    outputStandalone: true,
    images: {
      layoutRaw: true,
    },
  },
  images: {
    domains: ["testghost.mo.cloudinary.net"],
  },
}
