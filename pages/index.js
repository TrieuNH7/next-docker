import Head from "next/head"
import styles from "../styles/Home.module.css"
import Image from "next/image"

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>Mai Waifu Kanata</h1>

        <Image
          layout='raw'
          width={640}
          height={360}
          src='http://testghost.mo.cloudinary.net/image/upload/q_80/v1/gaftonosh/2021/10/-Darkhollow--Little-Busters--Ex-Epilogue-03V2--Bd--X264-1080P-Flac--10Bit--A971d0a5--1.m4v.00_01_03_10.Still004.webp'
        />

        <Image
          layout='raw'
          width={640}
          height={360}
          src='http://testghost.mo.cloudinary.net/image/upload/q_80/v1/gaftonosh/2021/10/2021-08-07-20_52_46-Greenshot-2.webp'
        />
      </main>

      <footer className={styles.footer}>
        <a
          href='https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app'
          target='_blank'
          rel='noopener noreferrer'
        >
          Powered by{" "}
          <img src='/vercel.svg' alt='Vercel Logo' className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}
